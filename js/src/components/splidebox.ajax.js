/**
 * @file
 * Provides Splidebox AJAX.
 */

(function ($, Drupal, _ds, _doc, _html) {

  'use strict';

  var ID = 'sbajx';
  var ID_ONCE = 'sboxAjax';
  var NICK = 'is-sbox';
  var IS_AJAXED = NICK + '-ajaxed';
  var C_TOUCH = 'touchevents';
  var C_NO_TOUCH = 'no-' + C_TOUCH;

  var SplideboxAjax = function (Splide, Components) {
    var ROOT = Splide.root;
    var OPTS = $.extend({}, Splide.options, $.parse(ROOT.dataset.splide));
    var AJAX = OPTS.ajax || {};
    var C_AJAX = AJAX.class;
    var S_AJAX = '.' + C_AJAX;
    var S_AJAX_ITEM = '.' + AJAX.item;
    var SLIDES = Components.Elements.slides;
    var SBOX;

    // @todo refactor or remove post #3031444, #3026636.
    function process(el) {
      var base = el.id;
      var href = el.href;
      var opts = {
        base: base,
        element: el,
        wrapper: base
      };

      if (href) {
        opts.url = href;
        opts.event = 'click';

        Drupal.ajax(opts);
      }
    }

    function onVisible(slide) {
      var item = slide.slide || SLIDES[slide] || SLIDES[Splide.index];
      var el;

      var clickIt = function () {
        el = $.find(item, S_AJAX);
        if ($.isElm(el) && !$.hasClass(el, C_AJAX + '-hit')) {

          el.click();
          // @todo use post #3031444, #3026636.
          // $.trigger(el, 'mousedown');
          $.addClass(el, C_AJAX + '-hit');
        }
      };

      clickIt();
      setTimeout(clickIt, 800);
    }

    function inject(el, x, y) {
      el.style.transform = 'translate3d(' + x + 'px, ' + (y || 0) + 'px, 0)';
    }

    return {
      dragon: function () {
        var opts = {};
        var el;
        var phase;
        var dir;

        function start(data) {
          el = data.el;

          el.style.transition = '';
        }

        function move(data) {
          dir = data.dir;
          el = data.el;

          if (dir === 'up' || dir === 'down') {
            inject(el, 0, data.y);
          }

          /*
          else {
            if (dir === 'left') {
              // Splide.go('<');
            }
            else if (dir === 'right') {
              // Splide.go('>');
            }
          }
          */
        }

        function end(data) {
          // $.removeClass(SBOX, IS_AJAXED + '--drag');
        }

        var callback = function (e, data) {
          el = data.el || el;
          phase = data.phase;

          if (phase === 'start') {
            start(data);
          }
          else if (phase === 'move') {
            move(data);
          }
          else if (phase === 'end') {
            end(data);
          }
        };

        function onWheel(e, data) {
          var el = data.el || e.target;

          if ($.hasClass(_html, C_NO_TOUCH)) {
            inject(el, 0, data.y, 1);
          }
        }

        opts.callback = callback;
        opts.nickClass = 'sbox';
        opts.activatedClass = IS_AJAXED;
        opts.vertical = true;
        opts.onWheel = onWheel;

        var items = [];
        $.each(SLIDES, function (slide) {
          var item = $.find(slide, S_AJAX_ITEM);

          if ($.isElm(item)) {
            items.push(item);
          }
        });

        if (items.length) {
          opts.elms = items;

          new SwipeDetect(SBOX, opts);
        }
      },

      mount: function () {
        var me = this;

        if ($.hasClass(ROOT, 'splide--nav') || !C_AJAX) {
          return;
        }

        SBOX = $.closest(ROOT, '.splidebox');

        $.once(process, ID_ONCE, S_AJAX, ROOT);

        Splide.on('visible.' + ID + ' move.' + ID, onVisible);

        // @todo remove post #3031444, #3026636.
        $.on(ROOT, 'click.' + ID, S_AJAX, function (e) {
          e.preventDefault();
          e.stopPropagation();
        }, false);

        // Must be outside splide events to support destroyed splide.
        if (AJAX.drag && !$.isTouch()) {
          me.dragon();
        }
      }

    };
  };

  _ds.extend({
    SplideboxAjax: SplideboxAjax
  });

})(dBlazy, Drupal, dSplide, this.document, this.document.documentElement);
