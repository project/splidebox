/**
 * @file
 * Provides Splidebox UI.
 */

(function ($) {

  'use strict';

  var _name = 'splidebox';
  var _overlay = _name + '__overlay';
  var _inner = _name + '__inner';
  var _content = _name + '__content';
  var _caption = _name + '__caption';
  var _thumbnail = _name + '__tn';
  var _nav = _name + '__nav';
  var _closer = _name + '__closer';
  var _counter = _name + '__counter';
  var _fullscreen = _name + '__fullscreen';
  var _slide = 'slide';
  var _slideContent = _slide + '__content';
  var _hide = 'aria-hidden="true"';
  var _polite = 'aria-live="polite"';
  var _divStart = '<div class=';
  var _divEnd = '</div>';
  var _ulStart = '<ul class=';
  var _ulEnd = '</ul>';
  var _liStart = '<li class=';
  var _liEnd = '</li>';
  var _btn = 'button';
  var _btnStart = '<' + _btn + ' class=';
  var _btnEnd = '</' + _btn + '>';

  var fn = Splidebox.prototype;
  fn.constructor = Splidebox;

  /**
   * The main lightbox HTML template.
   *
   * @param {Object} opts
   *   An object with the following keys: skin, fsIconOn.
   *
   * @return {String}
   *   Returns a html string.
   */
  fn.template = function (opts) {
    var me = this;
    var html;
    var skin = opts.skin ? ' ' + _name + '--skin--' + opts.skin : '';
    var closeIcon = '<svg aria-hidden="true" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg"><path d="M26.941 7.794 24.206 5.06 16 13.265 7.794 5.059 5.06 7.794 13.265 16l-8.206 8.206 2.735 2.735L16 18.735l8.206 8.206 2.735-2.735L18.735 16Z" style="stroke-width:1.36764"/></svg>';

    html = _divStart + '"$name $skin" tabindex="-1" aria-label="$name" $hide>';
    html += _divStart + '"$overlay">' + _divEnd;
    html += _divStart + '"$inner">';
    html += _divStart + '"$content">' + _divEnd;
    html += _divStart + '"$nav">';
    html += _divStart + '"$counter" $polite>' + _divEnd;
    html += _btnStart + '"$closer" type="$btn" $hide>' + closeIcon + _btnEnd;
    html += _btnStart + '"$fullscreen" type="$btn" data-fs-trigger $hide>$icon' + _btnEnd;
    html += _divEnd;

    if (opts.captionPos === 'overlay') {
      html += _divStart + '"$caption is-caption">' + _divStart + '"$caption--content">' + _divEnd + _divEnd;
    }

    if (opts.nav && me.count > 1) {
      html += _btnStart + '"$thumbnail" type="$btn" $hide><svg height="100" width="100" viewBox="0 0 206.518 206.518" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><path d="M94.696 12.498H15.924a3.424 3.424 0 0 0-3.426 3.426v78.773a3.424 3.424 0 0 0 3.426 3.425h78.773a3.424 3.424 0 0 0 3.425-3.425V15.924a3.426 3.426 0 0 0-3.426-3.426Zm-3.424 78.774H19.348V19.348h71.924Zm99.322-78.774h-78.773a3.424 3.424 0 0 0-3.425 3.426v78.773a3.424 3.424 0 0 0 3.425 3.425h78.773a3.424 3.424 0 0 0 3.426-3.425V15.924a3.425 3.425 0 0 0-3.426-3.426zm-3.425 78.774h-71.924V19.348h71.924zm-92.473 17.124H15.924a3.424 3.424 0 0 0-3.426 3.425v78.773a3.424 3.424 0 0 0 3.426 3.426h78.773a3.424 3.424 0 0 0 3.425-3.426v-78.773a3.426 3.426 0 0 0-3.426-3.425zm-3.424 78.773H19.348v-71.924h71.924zm99.322-78.773h-78.773a3.424 3.424 0 0 0-3.425 3.425v78.773a3.424 3.424 0 0 0 3.425 3.426h78.773a3.424 3.424 0 0 0 3.426-3.426v-78.773a3.425 3.425 0 0 0-3.426-3.425zm-3.425 78.773h-71.924v-71.924h71.924z" style="stroke-width:.878963"/></svg>' + _btnEnd;
    }
    html += _divEnd + _divEnd;

    return $.template(html, {
      btn: _btn,
      name: _name,
      skin: skin,
      overlay: _overlay,
      inner: _inner,
      content: _content,
      nav: _nav,
      counter: _counter,
      closer: _closer,
      fullscreen: _fullscreen,
      icon: opts.fsIconOn,
      hide: _hide,
      polite: _polite,
      caption: _caption,
      thumbnail: _thumbnail
    });
  };

  /**
   * Template for a complete splide wrapper.
   *
   * @return {String}
   *   Returns a html string.
   */
  fn.splideWrapper = function () {
    var me = this;
    var opts = me.options;
    var attrs = opts.attrs;
    var oWrapper = attrs.wrapper || {};
    var cWrapper = oWrapper.class;
    var oMain = attrs.main || attrs.default;
    var cMain = oMain.class || 'splide splide--default';
    var idMain = oMain.id;
    var oList = attrs.list || {};
    var cList = oList.class || 'splide__list';
    var oNav = attrs.nav || false;
    var cNav = oNav ? oNav.class : '';
    var idNav = oNav ? oNav.id : '';
    var nav = opts.nav && me.count > 1;
    var html = '';

    if (nav && cWrapper) {
      html += _divStart + '"$cWrapper">';
    }

    // Common markups.
    var base = _divStart + '"splide__arrows">' + _divEnd;
    base += _divStart + '"splide__slider">';
    base += _divStart + '"splide__track">';
    base += _ulStart + '"' + cList + '">';
    base += _liStart + '"splide__slide slide">' + _liEnd;
    base += _ulEnd;
    base += _divEnd;
    base += _divEnd;
    base += _ulStart + '"splide__pagination">' + _ulEnd;

    // Main slider.
    html += _divStart + '"$cMain" id="$idMain" data-blazy>';
    html += base;
    html += _divEnd; // cMain.

    // Nav slider.
    if (nav) {
      if (cNav) {
        html += _divStart + '"$cNav" id="$idNav" data-blazy>';
        html += base;
        html += _divEnd;
      }

      if (cWrapper) {
        html += _divEnd;
      }
    }

    return $.template(html, {
      cWrapper: cWrapper,
      cMain: cMain,
      idMain: idMain,
      cNav: cNav,
      idNav: idNav
    });
  };


  /**
   * Template for a slide.
   *
   * @param {Object} settings
   *   An object with the following keys: data, html.
   *
   * @return {String}
   *   Returns a html string.
   */
  fn.slideWrapper = function (settings) {
    var me = this;
    var opts = me.options;
    var data = settings.data;
    var content = settings.html;
    var caption = data.caption;
    var ajax = data.ajax;
    var ajaxUrl = ajax ? ajax.url : false;
    var ajaxOnly = ajax ? ajax.only : false;
    var captioned = caption && opts.captionPos === 'inline';
    var layout = captioned && opts.layout ? ' ' + _slide + '--caption--' + opts.layout : '';
    var ajaxClass = ajax.class;
    var ajaxSlide = ajaxUrl ? ' ' + ajax.slide : '';
    var ajaxItem = ajaxUrl ? ' ' + ajax.item : '';
    var ajaxId = ajaxClass + '-' + data.i;
    var html;

    html = _liStart + '"splide__slide $slideClass $layout $ajaxSlide">';
    html += _divStart + '"$slideContent">';
    html += _divStart + '"$name__item is-flex $ajaxItem">';

    if (!ajaxOnly) {
      if (captioned && content && !ajaxUrl) {
        html += _divStart + '"$slideClass__media is-flex">$content' + _divEnd;
        html += _divStart + '"$slideClass__caption is-centered is-caption" aria-live="polite">$caption' + _divEnd;
      }
      else {
        html += content;
      }
    }

    if (ajaxUrl) {
      // @todo use use-ajax post #3031444, #3026636.
      var link = '<a href="$ajaxUrl" class="$ajaxClass visually-hidden" data-ajax-wrapper="$ajaxId" id="$ajaxId">$ajaxUrl</a>';
      html += _divStart + '"$name__ajax">' + link + _divEnd;
    }

    html += _divEnd;
    html += _divEnd + _liEnd;

    return $.template(html, {
      slideContent: _slideContent,
      layout: layout,
      ajaxSlide: ajaxSlide,
      name: _name,
      ajaxItem: ajaxItem,
      content: content,
      caption: caption,
      ajaxUrl: ajaxUrl,
      ajaxClass: ajaxClass,
      ajaxId: ajaxId,
      slideClass: _slide
    });
  };

  /**
   * Template for a slide thumbnail.
   *
   * @param {Object} settings
   *   An object with the following keys: data, html.
   *
   * @return {String}
   *   Returns a html string.
   */
  fn.thumbnailWrapper = function (settings) {
    var data = settings.data;
    var alt = data.alt || 'thumbnail';
    var html;

    html = _liStart + '"splide__slide slide">';
    html += _divStart + '"slide__thumbnail">';
    html += '<img src="' + data.msrc + '" alt="' + alt + '" loading="lazy" decoding="async"/>';
    html += _divEnd + _liEnd;

    return html;
  };

  /**
   * Template for a (remote|local) video.
   *
   * @param {Object} settings
   *   An object with the following keys: data, html.
   *
   * @return {String}
   *   Returns a html string.
   */
  fn.htmlWrapper = function (settings) {
    var data = settings.data;
    var content = settings.html;
    var wrapper = 'media-wrapper';

    if (data.isPicture) {
      return content;
    }

    // @todo  style="width:$widthpx"
    var html = _divStart + '"$wrapper $wrapper--box">$content' + _divEnd;

    return $.template(html, {
      // width: data.width,
      content: content,
      wrapper: wrapper
    });
  };

})(dBlazy);
