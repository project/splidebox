/**
 * @file
 * Provides Splidebox, splide within lightbox.
 */

/* global define, module */
(function (root, factory) {

  'use strict';

  var ns = 'Splidebox';
  var db = root.dBlazy;
  var doc = root.document;
  var html = doc.documentElement;

  // Inspired by https://github.com/addyosmani/memoize.js/blob/master/memoize.js
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define([ns, db, Drupal, Eventify, root, doc, html], factory);
  }
  else if (typeof exports === 'object') {
    // Node. Does not work with strict CommonJS, but only CommonJS-like
    // environments that support module.exports, like Node.
    module.exports = factory(ns, db, Drupal, Eventify, root, doc, html);
  }
  else {
    // Browser globals (root is window).
    root[ns] = factory(ns, db, Drupal, Eventify, root, doc, html);
  }

}((this || module || {}), function (ns, $, Drupal, eventify, WIN, DOC, HTML) {

  'use strict';

  var BASE = 'splide';
  var NICK = 'sbox';
  var S_BASE = '.' + BASE;
  var NAME = BASE + 'box';
  var S_NAME = '.' + NAME;
  var ARIA_HIDDEN = 'aria-hidden';
  var V_PLAYABLE = 'playable';
  var IS_NICK = 'is-' + NICK;
  var IS_MOUNTED = IS_NICK + '-mounted';
  var IS_MOUNTING = IS_NICK + '-mounting';
  var IS_LOADING = 'is-b-loading is-b-visible';
  var IS_PLAYING = IS_NICK + '-playing';
  var IS_ACTIVE = IS_NICK + '-active';
  var IS_UMOUNTING = IS_NICK + '-umounting';
  var IS_FULLSCREEN = IS_NICK + '-fullscreen';
  var IS_PAGINATED = IS_NICK + '-paginated';
  var IS_TN = IS_NICK + '-thumbed';
  var IS_TN_VISIBLE = IS_TN + '--visible';
  var IS_MOVED = IS_NICK + '-moved';
  var IS_ZOOMABLE = 'is-zoomable';
  var IS_CENTERED = 'is-centered';
  // var IS_IMAGE = false;
  var IS_AJAX = false;
  var C_HTML = IS_NICK + '-html';
  var C_HTML_AJAX = C_HTML + '--ajaxed';
  var C_IS_PLAYABLE = IS_NICK + '-' + V_PLAYABLE;
  var C_IS_VISIBLE = IS_NICK + '-visible';
  var C_IS_CAPTIONED = 'is-captioned'; // @todo rename it: IS_NICK + '-captioned';
  var C_IS_AJAXED = IS_NICK + '-ajaxed';
  var C_IS_AJAXED_DRAG = C_IS_AJAXED + '--drag';
  var C_AJAX_SLIDE = 'is-ajax-slide';
  var C_VISUALLY_HIDDEN = 'visually-hidden';
  var S_SPLIDE_LIST = '.' + BASE + '__list';
  var S_DRAG_OBJ = '.' + NAME + '__item';
  var DATA = 'data-';
  var DATA_NAME = DATA + NAME;
  var V_MEDIA = 'media';
  var S_MEDIA = '.' + V_MEDIA;
  var DATA_MEDIA = DATA + NICK + '-' + V_MEDIA;
  var C_IMG = NAME + '__img';
  var V_IMG = 'img';
  var V_SRC = 'src';
  var V_SRCSET = 'srcset';
  var V_SIZES = 'sizes';
  var V_HTML = 'html';
  var V_TEMPLATE = 'template';
  var V_AUDIO = 'audio';
  var V_IFRAME = 'iframe';
  var V_VIDEO = 'video';
  var S_BLUR = '.b-blur';
  var S_IMG = V_IMG + ':not(' + S_BLUR + ')';
  var E_CLICK = 'click';
  var ADDCLASS = 'addClass';
  var REMOVECLASS = 'removeClass';
  var B_PROVIDER = 'b-provider--';
  var V_PROVIDER;
  var S_TRIGGER;
  var FN_SPLIDE = false;
  var FN_FULLSCREEN = false;
  var FN_MEDIA = false;
  var FN_ZOOM = false;
  var FN_SANITIZER = $.sanitizer;
  var V_OVERLAY = 'overlay';
  var V_WIDTH = 'width';
  var V_HEIGHT = 'height';
  var TRANSITIONEND = 'transitionend.' + NICK;
  var WIN_SIZE = {};
  var SUFFIXES = [
    V_OVERLAY,
    'content',
    'caption',
    'counter',
    'closer',
    'fullscreen',
    'inner'
  ];

  var DEFAULTS = {
    captionPos: V_OVERLAY,
    useNav: false,
    layout: 'bottom',
    trigger: '[' + DATA_NAME + '-trigger]',
    skin: '',
    skinNav: '',
    fsIconOn: '',
    init: false
  };

  var ITEM_DEFAULTS = {
    root: null,
    width: 0,
    height: 0,
    i: 0,
    html: '',
    alt: '',
    caption: '',
    msrc: '',
    src: '',
    srcset: '',
    type: '',
    rect: {},
    ajax: false
  };

  var FN;

  /**
   * Defines Splidebox constructor.
   *
   * @param {HTMLElement} el
   *   The Splidebox HTML element.
   * @param {object} opts
   *   The Splidebox options.
   *
   * @return {Splidebox}
   *   The Splidebox instance.
   *
   * @namespace
   */
  function Splidebox(el, opts) {
    var me = eventify($.extend({}, FN, this));
    var oMain = opts.main || {};

    me.name = ns;
    me.options = $.extend({}, DEFAULTS, opts);
    me.gallery = el;
    me.isSlider = $.hasClass(el, BASE) && !$.hasClass(el, 'is-carousel');
    me.ajax = oMain.ajax || {};
    me.ajax.slide = C_AJAX_SLIDE;

    // DOM ready fix.
    setTimeout(function () {
      WIN_SIZE = $.windowSize();

      init(me);
    });

    return me;
  }

  FN = Splidebox.prototype;
  FN.constructor = Splidebox;

  // Private Methods
  function rooting() {
    var me = this;
    var cn = me.root = createElement(V_TEMPLATE, me.template(me.options));

    $.each(SUFFIXES, function (key) {
      me[key] = $.find(cn, '.' + NAME + '__' + key);
    });

    return cn;
  }

  function buildRoot() {
    var me = this;
    var el = rooting.call(me);
    var frag = DOC.createDocumentFragment();

    append(frag, el);
    append(DOC.body, frag);

    $.addClass(el, IS_MOUNTING);
    $.addClass(me.content, IS_LOADING);

    $.trigger(WIN, NAME + ':mounted', {
      splidebox: me
    });
  }

  function buildSplide(resolve, reject) {
    var me = this;
    var opts = me.options;
    var cn = me.content;
    var tmp = createElement(V_TEMPLATE, me.splideWrapper());
    var slider = tmp;
    var sliderNav;
    var index = me.index;
    var useNav = opts.useNav && me.count > 1;
    var oMain = opts.main || {};
    var oNav = opts.nav;

    // Might be wrapped with splide-wrapper when having navigation.
    // Splidebox currently does not support slider with navigation.
    if ($.hasClass(tmp, BASE + '-wrapper')) {
      slider = $.find(tmp, '> ' + S_BASE + '--main');
    }

    if (useNav) {
      sliderNav = $.find(tmp, '> ' + S_BASE + '--nav');
      oNav.start = index;
      oNav.count = me.count;

      $.attr(sliderNav, 'data-' + BASE, JSON.stringify(oNav));
      me.sliderNav = sliderNav;

      $.addClass(me.root, IS_TN);
      if (opts.skinNav) {
        $.addClass(me.root, 'splidebox--navskin--' + opts.skinNav);
      }
    }
    else {
      $.removeClass(slider, BASE + '--main ' + BASE + '--nav');
    }

    oMain.start = index;
    oMain.count = me.count;
    $.attr(slider, 'data-' + BASE, JSON.stringify(oMain));

    append(cn, useNav ? tmp : slider);

    if (oMain.pagination) {
      $.addClass(me.root, IS_PAGINATED);
    }

    var list = me.list = $.find(slider, S_SPLIDE_LIST);
    var listNav = me.listNav = sliderNav ? $.find(sliderNav, S_SPLIDE_LIST) : null;
    me.slider = slider;

    if (!$.isElm(list)) {
      reject();
    }
    else {
      list.innerHTML = '';

      if (listNav) {
        listNav.innerHTML = '';
      }

      var sel = S_NAME + '__item:not(.is-ajax-item)';
      $.on(slider, E_CLICK + '.' + NICK, sel, function (e) {
        var dragging = $.hasClass(slider, 'is-dragging');
        if (e.target === this && !dragging) {
          var closer = me.closer || $.find(me.root, S_NAME + '__closer');

          if (closer) {
            closer.click();
          }
        }
      });

      $.each(me.items, function (item) {
        me.addSlide(item);
      });

      resolve();
    }
  }

  function showTime() {
    var me = this;
    var root = me.root;

    $.removeClass(root, IS_MOUNTING);
    $.addClass(root, IS_ACTIVE);
    $.addClass(HTML, C_HTML);
  }

  function hideTime() {
    var me = this;
    var root = me.root;

    if ($.isElm(root)) {
      $.attr(root, ARIA_HIDDEN, true);
      $.remove(root);
    }

    $.removeClass(HTML, [C_HTML, C_HTML_AJAX]);
  }

  function initSplide() {
    var me = this;
    var opts = me.options;
    var el = me.slider;
    var elNav = me.sliderNav;
    var s = {};
    var root = me.root;
    var index = me.index;
    var single = me.count === 1;

    s = $.extend({}, opts.main || {}, {
      zoomRoot: root
    });

    if (single) {
      $.addClass(root, 'is-sbox-unsplide');
    }

    if (opts.init) {
      s.start = index;
      s.count = me.count;

      // el.dataset.splide = JSON.stringify(s, getCircularReplacer());
      me.splide = FN_SPLIDE = opts.init(el, s);

      if (elNav) {
        $.addClass(root, IS_TN_VISIBLE);
        if ($.hasClass(elNav, 'is-less')) {
          // @todo remove when the library fixes this.
          var slides = $.findAll(elNav, '.slide');
          $.on(elNav, 'click.' + NICK, '.slide', function (e) {
            $.removeClass(slides, 'is-active');
            $.addClass(e.target, 'is-active');
          });
        }

        $.on(root, 'click.' + NICK, S_NAME + '__tn', function () {
          $.toggleClass(elNav, 'visually-hidden');
          $.toggleClass(root, IS_TN_VISIBLE);
        });
        me.splideNav = opts.init(elNav, s);
      }
    }

    me.emit('start', FN_SPLIDE);

    setTimeout(function () {
      if (FN_SPLIDE) {
        // me.curr = FN_SPLIDE.Components.Slides.getAt(index);
        me.currData = me.items[index];

        buildPreloader.call(me);

        // @fixme move it oout of here, not called here.
        FN_SPLIDE.on('ready.' + NICK, function (slide) {
          $.removeClass(me.content, IS_LOADING);

          me.emit('ready', FN_SPLIDE, slide);

          $.trigger(WIN, NAME + ':ready', {
            splidebox: me
          });
        });

        // Called here.
        FN_SPLIDE.on('active.' + NICK, function (slide) {
          me.index = FN_SPLIDE.index;
          me.curr = slide;
          me.currData = me.items[me.index];

          initPreloader.call(me);

          decode.call(me, me.currData);

          setCaption.call(me);

          var provider = me.currData.provider;
          $.attr(root, DATA_MEDIA, me.currData.type);

          if (V_PROVIDER) {
            $.removeClass(root, B_PROVIDER + V_PROVIDER);
          }
          if (provider) {
            $.addClass(root, B_PROVIDER + provider);
          }

          // IS_IMAGE = me.currData.type === 'image';
          IS_AJAX = typeof me.currData.ajax.url !== 'undefined';
          // $[IS_IMAGE ? ADDCLASS : REMOVECLASS](me.inner, 'is-b-loading is-b-visible');
          $[IS_AJAX ? ADDCLASS : REMOVECLASS](root, C_IS_AJAXED);
          $[IS_AJAX ? ADDCLASS : REMOVECLASS](HTML, C_HTML_AJAX);
          $[IS_AJAX && me.ajax.drag ? ADDCLASS : REMOVECLASS](root, C_IS_AJAXED_DRAG);

          $[me.currData.display === V_PLAYABLE ? ADDCLASS : REMOVECLASS](root, C_IS_PLAYABLE);

          V_PROVIDER = provider;

          $.trigger(WIN, NAME + ':active', {
            splidebox: me,
            slide: slide
          });

          setTimeout(function () {
            me.emit('active', FN_SPLIDE, slide);
          }, 100);
        });

        // FN_SPLIDE.on('move.' + NICK, function (newIndex, prevIndex, destIndex) {
        // $.removeClass(root, IS_SBOX_ZOOMED)
        // });
        FN_SPLIDE.on('moved.' + NICK, function (newIndex, prevIndex, destIndex) {
          clearDragMarkers.call(me);

          me.emit('moved', newIndex, prevIndex, destIndex);

          $.trigger(WIN, NAME + ':moved', {
            splidebox: me,
            newIndex: newIndex,
            prevIndex: prevIndex,
            destIndex: destIndex
          });
        });

        // After captions are loaded.
        // @todo use an event after `active` fire instead, if any.
        setTimeout(function () {
          if (single) {
            FN_SPLIDE.destroy();
          }
        }, 100);

        $.trigger(WIN, NAME + ':init', {
          splidebox: me,
          splide: FN_SPLIDE
        });
      }
    });
    return FN_SPLIDE;
  }

  function buildPreloader() {
    var me = this;
    var data = me.currBox = me.toBox();

    if (data && !data.hasImg) {
      return;
    }

    setClone(me, data, true);

    me.zoom('off');
    var clone = me.clone;

    if (!clone) {
      me.shouldPreload = false;
      return;
    }

    var called = false;
    var reset = function (e) {
      var _style = clone.style;
      _style.transform = 'translate3d(0, 0, 0) scale3d(1, 1, 1)';

      $.off(clone, TRANSITIONEND, reset);

      me.zoom('on');

      setTimeout(function () {
        showTime.call(me);
        $.remove(clone);
        me.clone = null;
      }, 101);

      called = true;
    };

    $.on(clone, TRANSITIONEND, reset);

    setTimeout(function () {
      if (!called) {
        reset();
      }
    }, 1200);

    me.shouldPreload = true;
  }

  function initPreloader() {
    var me = this;

    setTimeout(function () {
      var box = me.currBox;
      var img = box.img.el;

      if (box.isValid && $.isElm(img)) {
        if ($.equal(img, 'img')) {

          if (img.complete) {
            transformPreloader.call(me);
          }
          else {
            $.on(img, 'load', transformPreloader.bind(me));
          }
        }
      }
      else {
        showTime.call(me);
      }
    });
  }

  function transformPreloader() {
    var me = this;
    var ws = WIN_SIZE;
    var box = me.currBox;
    var img = box.img.el;
    var ox = 1;
    var oy = 1;
    var margin = 0;
    var sub = 0.5;
    var subWin = 2;
    var rect = $.rect(img);
    var iw = $.outerWidth(img);
    var ih = $.outerHeight(img);

    box = $.extend({}, box, {
      img: {
        el: img,
        width: iw,
        height: ih,
        left: rect.left,
        top: rect.top,
        rect: rect
      }
    });

    setTimeout(function () {
      setClone(me, box);

      if (!me.clone) {
        return;
      }

      var item = box.img;
      var aox = Math.abs(ox);
      var aoy = Math.abs(oy);
      var ix =
        (ox > 0 ? 1 - aox : aox) * ws.width +
        (ox * ws.width) / subWin -
        item.left -
        sub * iw;
      var iy =
        (oy > 0 ? 1 - aoy : aoy) * ws.height +
        (oy * ws.height) / subWin -
        item.top -
        sub * ih;
      var iz = Math.min(
        Math.min(ws.width * aox - margin, item.width - margin) / iw,
        Math.min(ws.height * aoy - margin, item.height - margin) / ih
      );

      ix += 4;

      me.clone.style.transform =
        'translate3d(' +
        ix +
        'px, ' +
        iy +
        'px, 0) scale3d(' +
        iz +
        ', ' +
        iz +
        ', 1)';
    }, 101);
  }

  function attachRoot(splide) {
    var me = this;

    setTimeout(function () {
      initPlugins.call(me);
      var splide = me.splide;

      if (splide) {
        splide.refresh();
      }
    });
  }

  function buildOut() {
    var me = this;

    if (!me.count) {
      return;
    }

    buildRoot.call(me);

    var promise = new Promise(buildSplide.bind(me));
    promise.then(initSplide.bind(me));
    promise.then(attachRoot.bind(me));
  }

  function toggleCaption(e) {
    $.toggleClass(e.target, C_IS_VISIBLE);
  }

  function onPlaying() {
    $.addClass(this.root, IS_PLAYING);
  }

  function onStopped() {
    $.removeClass(this.root, IS_PLAYING);
  }

  function checkVH(e) {
    WIN_SIZE = $.windowSize();
    var vh = WIN_SIZE.height / 100;

    HTML.style.setProperty('--vh', vh + 'px');
  }

  function initEvents() {
    var me = this;
    var die = me.close.bind(me);

    $.on(WIN, 'resize.' + NICK + ' orientationchange.' + NICK, $.debounce(checkVH, 301));
    $.on(me.closer, E_CLICK, die);
    $.on(me.overlay, E_CLICK, die);
    $.on(me.caption, E_CLICK, toggleCaption);

    $.on(WIN, 'blazy:mediaPlaying', onPlaying.bind(me));
    $.on(WIN, 'blazy:mediaStopped', onStopped.bind(me));

    $.on(DOC.body, 'keydown', function (e) {
      if (e.key === 'Escape') {
        me.close();
      }
    });
  }

  function initPlugins() {
    var me = this;
    var splide = me.splide;

    if (splide && splide.Components) {
      var components = splide.Components;

      if (!FN_FULLSCREEN && 'xFullScreen' in components) {
        FN_FULLSCREEN = components.xFullScreen;

        var fsOptions = {
          element: me.root,
          className: IS_FULLSCREEN
        };
        FN_FULLSCREEN.init(fsOptions);
      }

      if (!FN_MEDIA && 'xMedia' in components) {
        FN_MEDIA = components.xMedia;
      }

      if (!FN_ZOOM && 'xZoom' in components) {
        FN_ZOOM = components.xZoom;
      }
    }
  }

  function createElement(tagName, html, className, root, attach) {
    attach = attach || '';
    var el = $.create(tagName || 'div', className, html);

    // @todo replace with dBlazy post Blazy 2.6.
    if (root && attach) {
      if (attach === 'prepend') {
        root.insertBefore(el, root.childNodes[0] || null);
      }
      else if (attach === 'append') {
        append(root, el);
      }
      else if (attach === 'next') {
        root.insertAdjacentElement('afterend', el);
      }
    }
    return el;
  }

  function clearDragMarkers() {
    var root = this.root;

    setTimeout(function () {
      $.removeAttr(root, DATA + 'scale');

      var moves = $.findAll(root, S_DRAG_OBJ);
      if (moves.length) {
        $.each(
          moves,
          function (item) {
            item.style.transform = '';

            $.removeClass(item, IS_MOVED);
          },
          100
        );
      }
    });
  }

  function setClone(me, data, initial) {
    var clone;
    var img = data.img;
    var tn = data.tn;
    var source = initial ? tn : img;
    var height = source.height;
    var width = source.width;
    var left = source.left;
    var top = source.top;
    var src = tn.src;

    if (initial) {
      clone = createElement('div', null, NAME + '__clone', me.overlay, 'next');
      me.clone = clone;
    }

    if (src && me.clone) {
      var _style = me.clone.style;
      _style.height = height + 'px';
      _style.width = width + 'px';
      _style.left = left + 'px';
      _style.top = top + 'px';

      _style.backgroundImage = 'url(' + src + ')';
    }
  }

  function setCaption() {
    var me = this;
    var opts = me.options;
    var el = me.caption;
    var data = me.currData;
    var captioned = $.isObj(data) &&
      data.caption &&
      !data.ajax;

    if ($.isElm(el) && opts.captionPos === V_OVERLAY) {
      var text = captioned ? FN_SANITIZER.sanitize(data.caption) : '';
      var content = $.find(el, S_NAME + '__caption--content');
      if (content) {
        content.innerHTML = text;
        $[captioned ? ADDCLASS : REMOVECLASS](el, C_IS_CAPTIONED);
      }
    }

    if (me.counter) {
      me.counter.innerText = me.index + 1 + '/' + me.splide.length;
    }
  }

  function preload(data) {
    var _ajax = data.ajax ? '' : ' ' + IS_ZOOMABLE;
    var image = createElement(V_IMG, null, C_IMG + _ajax);
    var attrs = ['alt', V_SRC, V_SRCSET, V_SIZES, V_HEIGHT, V_WIDTH];

    if (data.src) {
      image.decoding = 'async';
      image.loading = 'lazy';

      $.each(attrs, function (attr) {
        if (attr in data && data[attr]) {
          image[attr] = data[attr];
        }
      });
    }
    data.img = image;
    return image;
  }

  function decode(data) {
    var me = this;
    var slide = data.slide;
    var image = $.find(slide, 'img.' + IS_ZOOMABLE);
    var slider = me.slider;
    var img;

    if (!image || $.isDecoded(image)) {
      return;
    }

    img = new Image();
    img.decoding = 'async';

    $.each([V_SRC, V_SRCSET], function (attr) {
      if (attr in data && data[attr]) {
        img[attr] = data[attr];
      }
    });

    if (slider && me.count > 1) {
      $.addClass(slider, IS_LOADING);
    }

    $.decode(img)
      .then(function () {
        $.removeClass(slider, IS_LOADING);
      })
      .catch(function () {
        $.removeClass(slider, IS_LOADING);
      });
  }

  // Normally thumbnails, unless a slider, and so the .splide container.
  function projected(timg, iw, src) {
    var ws = WIN_SIZE;
    var ww = ws.width;
    var wh = ws.height;
    var ox = 1;
    var oy = 1;
    var tRect = $.rect(timg);
    var th = $.outerHeight(timg);
    var tw = $.outerWidth(timg);
    var tl = tRect.left;
    var tt = tRect.top;
    var aox = Math.abs(ox);
    var aoy = Math.abs(oy);
    var tminX = (ox > 0 ? 1 - aox : aox) * ww + (ox * ww) / 2;
    var tminY = (oy > 0 ? 1 - aoy : aoy) * wh + (oy * wh) / 2;
    var tx = tl + tw / 2 - tminX;
    var ty = tt + th / 2 - tminY;
    var tz = tw / iw;

    return {
      el: timg,
      src: src,
      x: tx,
      y: ty,
      z: tz,
      height: th,
      width: tw,
      left: tl,
      top: tt,
      rect: tRect
    };
  }

  function stopVideo() {
    if (FN_MEDIA) {
      FN_MEDIA.close();
    }
  }

  function extractAttr(html, attr) {
    return html
      .split(attr + '="')
      .pop()
      .split('"')[0];
  }

  function detach(me) {
    var splide = me.splide;

    if (splide) {
      stopVideo.call(me);

      FN_FULLSCREEN = false;
      FN_MEDIA = false;
      FN_ZOOM = false;
      splide.destroy(true);
      splide = null;
    }

    hideTime.call(me);

    $.trigger(WIN, NAME + ':detach', {
      splidebox: me,
      splide: FN_SPLIDE
    });
  }

  function append(parent, el) {
    if ($.isElm(parent)) {
      if ($.isElm(el)) {
        parent.appendChild(el);
      }
      else {
        $.append(parent, el);
      }
    }
  }

  function launch(index) {
    var me = this;
    var root;

    me.index = index;

    buildOut.call(me);
    initEvents.call(me);

    root = me.root;

    if (root) {
      $.addClass(root, IS_MOUNTED);
      $.attr(root, ARIA_HIDDEN, false);

      setTimeout(function () {
        $.removeClass(me.content, IS_LOADING);
      }, 1200);
    }
  }

  function initials(me) {
    me.root = null;

    $.each(SUFFIXES, function (key) {
      me[key] = null;
    });

    me.index = 0;
    me.count = 0;
    me.items = [];
    me.list = null;
    me.slider = null;
    me.triggers = [];
    me.shouldPreload = false;
    me.splide = null;
    me.curr = null;
    me.currBox = {
      img: {},
      tn: {}
    };
    me.currData = {};
  }

  function init(me) {
    checkVH();

    initials(me);

    S_TRIGGER = me.options.trigger;

    var gallery = me.gallery;
    if (gallery && $.isElm(gallery)) {
      $.on(gallery, E_CLICK, S_TRIGGER, me.open.bind(me), false);
    }
  }

  /**
   * Gets the current clicked item index.
   *
   * @param {HTMLElement} link
   *   The link HTML element.
   *
   * @return {Int}
   *   The current clicked link index.
   */
  FN.getIndex = function (link) {
    var me = this;
    var i = 0;
    if (me.items.length) {
      $.each(me.items, function (data, idx) {
        if (data.element && data.element === link) {
          i = idx;
          return i;
        }
      });
    }

    return i;
  };

  FN.go = function (index) {
    if (FN_SPLIDE) {
      FN_SPLIDE.go(index);
    }
    return this;
  };

  FN.toData = function (el) {
    var me = this;
    var data = $.parse($.attr(el, 'data-b-media data-media'));
    var div = $.find(el, S_MEDIA);
    var href = $.attr(el, 'href');
    var url = $.attr(el, DATA + 'box-url', href, true);
    var img = $.find(el, S_IMG);
    var check = el.nextElementSibling;
    var caption = $.hasClass(check, C_VISUALLY_HIDDEN) ? check : null;
    var rect = $.rect(el);
    var ajaxUrl = $.attr(el, DATA + 'b-ajax');
    var html = '';
    var type = data.boxType || '';
    var display = type;
    var alt = $.attr(img, 'alt');
    var isResimage;
    var isPicture;

    if ([V_IFRAME, V_AUDIO, V_VIDEO].includes(type)) {
      display = V_PLAYABLE;
    }
    if (ajaxUrl) {
      display = V_HTML;
    }

    var item = {
      element: el,
      width: $.toInt(data.width, 640),
      height: $.toInt(data.height, 360),
      i: me.getIndex(el),
      alt: FN_SANITIZER.sanitize(alt),
      caption: $.isElm(caption) ? caption.innerHTML : '',
      msrc: $.attr(div, 'data-b-thumb data-thumb'),
      provider: data.provider,
      type: type,
      rect: rect,
      isPicture: false,
      ajax: false,
      display: display
    };

    if (ajaxUrl) {
      item.ajax = $.extend(me.ajax, {
        url: ajaxUrl
      });
    }

    if (V_HTML in data) {
      html = data.html;

      if (data.encoded) {
        html = atob(html);
      }

      // Only supports non-picture, unfortunately.
      isResimage = $.contains(html, V_SRCSET);
      isPicture = $.contains(html, '<picture');
      item.isPicture = isPicture;

      if (isResimage && !isPicture) {
        item.srcset = extractAttr(html, V_SRCSET);
        item.src = extractAttr(html, V_SRC);
        item.sizes = extractAttr(html, V_SIZES);
      }
      else {
        if ([V_AUDIO, V_VIDEO].includes(type)) {
          item.src = url;
        }
        // Local video and picture.
        item.html = me.htmlWrapper({
          html: html,
          data: item
        });
      }
    }
    else if (type === V_IFRAME) {
      item.html = Drupal.theme('blazyMedia', {
        el: el
      });
    }
    else {
      item.src = url;
    }

    if (!item.msrc) {
      item.msrc = item.src;
    }

    return $.extend({}, ITEM_DEFAULTS, item);
  };

  FN.addSlide = function (data) {
    var me = this;
    var el = me.list;
    var elNav = me.listNav;
    var slide = me.toSlide(data);

    if ($.isElm(el)) {
      append(el, slide);

      data.slide = slide;
      $.trigger(WIN, NAME + ':added', {
        splidebox: me,
        data: data
      });
    }

    if ($.isElm(elNav)) {
      slide = me.toThumbnail(data);

      append(elNav, slide);
      data.slideNav = slide;
    }
  };

  FN.toSlide = function (data) {
    var me = this;
    var content;
    var img;

    if (data.html) {
      content = data.html;
    }
    else {
      if (data.src) {
        img = preload(data);
        content = img.outerHTML;
      }
    }

    var html = me.slideWrapper({
      html: content,
      data: data
    });

    var el = createElement(V_TEMPLATE, html);
    var picture = $.find(el, 'picture');

    if ($.isElm(picture) && !data.ajax) {
      var dim = WIN_SIZE;
      img = $.find(picture, 'img');

      $.addClass(picture, IS_CENTERED);
      $.addClass(img, C_IMG + ' ' + IS_ZOOMABLE);
      $.attr(img, 'loading', 'lazy');
      $.attr(img, V_WIDTH, (dim.width - 32));
      $.attr(img, V_HEIGHT, (dim.height - 22));
    }
    return el;
  };

  FN.toThumbnail = function (data) {
    var me = this;
    var html = me.thumbnailWrapper({
      data: data
    });

    return createElement(V_TEMPLATE, html);
  };

  FN.toBox = function () {
    var me = this;
    var data = me.currData || me.items[me.index];

    if ($.isUnd(data)) {
      return {};
    }

    var link = data.element;
    var slide = $.isEmpty(me.curr) ? data.slide : me.curr.slide;
    var img = $.find(slide, S_IMG);
    var timg = $.find(link, S_IMG);
    var elProjected = me.isSlider ? me.gallery : timg;
    var ih = 0;
    var iw = 0;
    var il = 0;
    var it = 0;
    var iRect = {};
    var isAjax = data.ajax;
    var isIframe = data.type === V_IFRAME;
    var isVideo = data.type === V_VIDEO;
    var projection = {};

    if (!$.isElm(img)) {
      var video = $.find(slide, V_VIDEO);
      var poster = $.attr(video, 'poster');

      if (poster) {
        img = video;
      }
    }

    if ($.isElm(img)) {
      iRect = $.rect(img);
      ih = $.outerHeight(img);
      iw = $.outerWidth(img);
      il = iRect.left;
      it = iRect.top;
    }

    if ($.isElm(elProjected)) {
      projection = projected(elProjected, iw, data.msrc || timg.src);
    }

    var box = {
      hasImg: $.isElm(img),
      hasTn: $.isElm(timg),
      isAjax: isAjax,
      isValid: $.isElm(img) && $.isElm(timg),
      isIframe: isIframe,
      isVideo: isVideo,
      isPlayable: isIframe || isVideo,
      img: {
        el: img,
        height: ih,
        width: iw,
        left: il,
        top: it,
        rect: iRect
      },
      tn: projection
    };

    me.currBox = box;
    return box;
  };

  FN.zoom = function (e) {
    if (FN_ZOOM) {
      FN_ZOOM[e]();
    }
  };

  FN.destroy = function () {
    var me = this;
    var root = me.root;

    $.addClass(root, IS_UMOUNTING);

    var box = me.toBox();

    // || box.isPlayable || box.isAjax
    if (!box || !box.isValid) {
      detach(me);
      return;
    }

    var img = box.img.el;
    if (!img) {
      detach(me);
      return;
    }

    var tn = box.tn;
    var z = tn.z;

    me.zoom('off');

    $.addClass(img, NAME + '__trans');
    img.style.transform =
      'translate3d(' +
      tn.x +
      'px, ' +
      tn.y +
      'px, 0) scale3d(' +
      z +
      ', ' +
      z +
      ', 1)';

    var called = false;
    var onEnded = function (e) {
      img.style.transform = 'translate3d(0,0,0) scale3d(1,1,1)';

      called = true;

      detach(me);
    };

    $.one(img, TRANSITIONEND, onEnded);

    $.off('blazy:mediaPlaying', onPlaying.bind(me));
    $.off('blazy:mediaStopped', onStopped.bind(me));

    // Failsafe with potential screwed-up transition.
    setTimeout(function () {
      if (!called) {
        detach(me);
      }
    }, 800);

    $.trigger(WIN, NAME + ':destroy', {
      splidebox: me,
      splide: FN_SPLIDE
    });
  };

  FN.close = function () {
    var me = this;

    // if ($.hasClass(me.root, IS_FULLSCREEN)) {
    // if (FN_FULLSCREEN) {
    // me.fullscreen.click();
    // }
    // Exiting via browser Exit button.
    // if (!DOC.fullscreenElement) {
    // me.destroy();
    // }
    // return false;
    // }

    me.destroy();
  };

  FN.open = function (e) {
    var me = this;
    e.preventDefault();
    e.stopPropagation();

    initials(me);

    // With a mix of (non-)lightboxed contents: image, video, Facebook,
    // Instagram, etc., some may not always be lightboxed, so filter em out.
    var target = e.target;
    var parent = $.closest(target, 'a');
    var link = $.attr(parent, 'href') ? parent : $.closest(target, S_TRIGGER);
    var triggers = [];
    var pos;

    if ($.isStr(S_TRIGGER)) {
      triggers = $.findAll(me.gallery, S_TRIGGER);
    }

    me.count = triggers.length;
    me.triggers = triggers;

    if (me.count) {
      $.each(triggers, function (el, i) {
        var clone = $.closest(el, S_BASE + '__slide--clone');
        if (!$.isElm(clone)) {
          var data = me.toData(el);
          data.i = i;
          me.items.push(data);
        }
      });
    }

    pos = me.getIndex(link);
    // $.addClass(HTML, C_HTML);
    launch.call(me, pos);
  };

  /**
   * Initializing the HTML element into a Splidebox.
   *
   * @param {HTMLElement} el
   *   The Splidebox HTML element.
   * @param {object} opts
   *   The Splidebox options.
   *
   * @return {function}
   *   The Splidebox instance.
   */
  Splidebox.init = function (el, opts) {
    if (!el.splidebox) {
      el.splidebox = new Splidebox(el, opts);
    }
    return el.splidebox;
  };

  return Splidebox;

}));
