<?php

/**
 * @file
 * Post update hooks for Splidebox.
 */

use Symfony\Component\Yaml\Yaml;

/**
 * Import a config entity.
 */
function _splidebox_import($module, $name, $key, $folder = 'install') {
  $config_factory = \Drupal::configFactory();
  $path = blazy()->getPath('module', $module);
  $config_path = sprintf('%s/config/%s/%s.optionset.%s.yml', $path, $folder, $name, $key);
  $data = Yaml::parseFile($config_path);

  $config_factory->getEditable(sprintf('%s.optionset.%s', $name, $key))
    ->setData($data)
    ->save(TRUE);
}

/**
 * Added thumbnail navigation.
 */
function splidebox_post_update_added_thumbnail_navigation() {
  $config = \Drupal::configFactory()->getEditable('blazy.settings');
  $config->set('extras.splidebox_nav', 'splidebox_nav');
  $config->save(TRUE);

  _splidebox_import('splidebox', 'splide', 'splidebox');
  _splidebox_import('splidebox', 'splide', 'splidebox_nav');
}

/**
 * Updated main and nav optionsets for Splide:2.0.4.
 */
function splidebox_post_update_updated_optionsets() {
  _splidebox_import('splidebox', 'splide', 'splidebox');
  _splidebox_import('splidebox', 'splide', 'splidebox_nav');
}

/**
 * Updated main and nav optionsets for Splide:2.0.4 rewind options.
 */
function splidebox_post_update_added_rewind_options() {
  _splidebox_import('splidebox', 'splide', 'splidebox');
  _splidebox_import('splidebox', 'splide', 'splidebox_nav');
}

/**
 * Removed height 100vh to support dynamic mobile viewport.
 */
function splidebox_post_update_removed_height_100vh() {
  _splidebox_import('splidebox', 'splide', 'splidebox');
}
