<?php

namespace Drupal\splidebox\Plugin\splide;

use Drupal\splide\SplideSkinPluginBase;

/**
 * Provides splide skins for lightboxes.
 *
 * @SplideSkin(
 *   id = "splidebox_skin",
 *   label = @Translation("Splidebox skin")
 * )
 */
class SplideboxSkin extends SplideSkinPluginBase {

  /**
   * Sets the splide skins.
   *
   * @inheritdoc
   */
  protected function setSkins() {
    $path = $this->getPath('module', 'splidebox');

    // If you copy this file, be sure to add base_path() before any asset path
    // (css or js) as otherwise failing to load the assets. Your module can
    // register paths pointing to a theme. Check out splide.api.php for details.
    $skins = [
      'sbox-nav' => [
        'name' => 'Splidebox: nav',
        'description' => $this->t('Splidebox navigation skin.'),
        'css' => [
          'theme' => [
            $path . '/css/theme/splidebox.theme--sbox-nav.css' => [],
          ],
        ],
      ],
    ];

    foreach ($skins as $key => $skin) {
      $skins[$key]['group'] = strpos($key, 'nav') === FALSE ? 'main' : 'nav';
      $skins[$key]['provider'] = 'splidebox';
    }

    return $skins;
  }

}
