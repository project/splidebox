<?php

namespace Drupal\splidebox;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Drupal\blazy\Field\BlazyField;
use Drupal\splide\Entity\Splide;
use Drupal\splide\Form\SplideAdminInterface;
use Drupal\splide\SplideManager;

/**
 * Adds Splidebox functionality.
 */
class Splidebox extends SplideManager implements SplideboxInterface {

  /**
   * The splide admin service.
   *
   * @var \Drupal\splide\Form\SplideAdminInterface
   */
  protected $admin;

  /**
   * The lightbox dummy template.
   *
   * @var string
   */
  private $template = '';

  /**
   * {@inheritdoc}
   */
  public function setAdmin(SplideAdminInterface $admin): self {
    $this->admin = $admin;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function toAttachments(array &$load, array &$attach): void {
    $this->verifySafely($attach);

    if ($splideboxes = $attach['splideboxes'] ?? NULL) {
      if ($skin = $splideboxes->get('skin')) {
        $attach['skin'] = $skin;
      }

      if ($splideboxes->get('useNav')) {
        $load['library'][] = 'splidebox/nav';
        if ($skin = $splideboxes->get('skinNav')) {
          $attach['skin_nav'] = $skin;
        }
      }
    }

    $this->skinManager->attachCore($load, $attach);

    $load['library'][] = 'splidebox/load';
    if (!empty($attach['ajax_link'])) {
      $load['library'][] = 'splidebox/ajax';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function attachAlter(array &$load, array &$attach = []): void {
    $this->toAttachments($load, $attach);

    $this->moduleHandler->alter('splidebox_attach', $load, $attach);
  }

  /**
   * {@inheritdoc}
   */
  public function preprocessBlazy(array &$variables): void {
    $settings = &$variables['settings'];

    // Provides AJAX contents if so configured.
    if (!empty($settings['ajax_link'])) {
      $this->toAjaxUrl($variables);
    }
  }

  /**
   * {@inheritdoc}
   *
   * @todo remove non-object definition post migrations at/by blazy:3.x.
   */
  public function formElementAlter(array &$form, array $definition): void {
    $blazies     = $definition['blazies'] ?? NULL;
    $settings    = $definition['settings'] ?? [];
    $namespaces  = array_keys(self::supportedModules());
    $namespace   = $definition['namespace'] ?? '';
    $field_type  = $definition['field_type'] ?? '';
    $target_type = $definition['target_type'] ?? '';
    $bundles     = $definition['target_bundles'] ?? [];
    $links       = ['link', 'string'];
    $entities    = ['entity_reference', 'entity_reference_revisions'];

    // @todo remove checks post blazy:2.17.
    if ($blazies) {
      $bundles     = $blazies->get('field.target_bundles', []) ?: $bundles;
      $field_type  = $blazies->get('field.type') ?: $field_type;
      $target_type = $blazies->get('target_type') ?: $target_type;
      $namespace   = $blazies->get('namespace') ?: $namespace;
    }

    $supported  = $namespace && in_array($namespace, $namespaces);
    $applicable = $supported && ($field_type && in_array($field_type, $entities));
    if ($applicable && isset($form['media_switch'])) {
      $options = $this->admin->getFieldOptions($bundles, $links, $target_type);
      $form['ajax_link'] = [
        '#type'          => 'select',
        '#title'         => $this->t('Lightbox AJAX link'),
        '#options'       => $options,
        '#empty_option'  => $this->t('- None -'),
        '#default_value' => $settings['ajax_link'] ?? '',
        '#weight'        => -98.99,
        '#states'        => [
          'visible' => [
            'select[name*="[media_switch]"]' => ['value' => 'splidebox'],
          ],
        ],
        '#description'   => $this->t('Choose any single-value link field types which have urls (<code>/node/123</code>) to nodes (other entities or forms are not supported to avoid complications). It will be loaded as the lightbox AJAX contents. Specific for Media, be sure the same link field available for all media types to have a mixed of Media correctly. To replace images/ videos with AJAX contents only, override <code>box_ajax_only</code> option to TRUE via <code>hook_splidebox_attach_alter</code>. Default is FALSE, the non-zoomable lightbox image and video are embedded above AJAX contents as hybrid contents.'),
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions(Splide $optionset, $count = 1, $main = TRUE): array {
    $options  = $optionset->getSettings();
    $excludes = ['classes', 'i18n', 'padding'];
    $excludes = array_combine($excludes, $excludes);
    $options  = array_diff_key($options, $excludes);
    $video    = empty($options['video']) ? [] : (array) $options['video'];
    $autoplay = $video['autoplay'] ?? TRUE;

    if ($main) {
      // Some are hidden module features. Wheel is inherent with Zoom, disable.
      $options['video'] = $video;
      $options['video']['autoplay'] = $autoplay;
      $options['wheel'] = FALSE;

      $zoom = $this->getZoomOptions();
      $zooms = empty($options['zoom']) ? [] : (array) $options['zoom'];
      $options['zoom'] = array_merge($zoom, $zooms);
      $options['fullscreen'] = TRUE;
    }

    // @todo recheck 0, since it shouldn't be called nor displayed if 0.
    $options['count'] = $count ?: 1;
    return $optionset->toJson($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getOptionset(array $settings): Splide {
    $name = $this->getOptionsetName($settings);

    return Splide::loadSafely($name);
  }

  /**
   * Checks if splidebox is applicable.
   */
  public function isApplicable(array &$settings): bool {
    $blazies = $settings['blazies'] ?? NULL;
    $switch  = $settings['media_switch'] ?? '';

    if ($switch == 'splidebox') {
      // Tell Blazy we are supporting rich box: local video within lightbox.
      if ($blazies) {
        $blazies->set('is.richbox', TRUE)
          ->set('is.encodedbox', TRUE);
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function toAttributes(array &$settings): void {
    $blazies = $settings['blazies'];
    $options = $this->toDataset($settings);

    $options = base64_encode(Json::encode($options));
    $blazies->set('data.splidebox', $options);
  }

  /**
   * Returns the supported modules to have AJAX links.
   */
  public static function supportedModules(): array {
    return [
      'blazy'     => 'Drupal\blazy\BlazyDefault',
      'gridstack' => 'Drupal\gridstack\GridStackDefault',
      'mason'     => 'Drupal\mason\MasonDefault',
      'outlayer'  => 'Drupal\outlayer\OutlayerDefault',
      'splide'    => 'Drupal\splide\SplideDefault',
    ];
  }

  /**
   * Returns the zoom options.
   */
  private function getZoomOptions(): array {
    return [
      'on'          => TRUE,
      'click'       => TRUE,
      'scale'       => TRUE,
      'min'         => 1,
      'max'         => 1.5,
      'root'        => '.splidebox',
      'nickClass'   => 'sbox',
      'minWidth'    => '768px',
      'target'      => '.splidebox__item:not(.is-ajax-item)',
      'transform'   => TRUE,
    ];
  }

  /**
   * Returns the optionset name.
   */
  private function getOptionsetName(array $settings): string {
    return $settings['splidebox'] ?? 'splidebox';
  }

  /**
   * Overrides variables for theme_blazy().
   */
  private function toAjaxUrl(array &$variables): void {
    $settings = $variables['settings'];
    $blazies  = $settings['blazies'];
    $langcode = $blazies->get('language.current');
    $box_url  = $blazies->get('lightbox.url');
    $item     = $blazies->get('image.item', $variables['item'] ?? NULL);

    // Node: $node = $blazies->get('entity.instance');
    // Media: $media = $blazies->get('media.instance');
    // Below is a media entity by designed:
    $entity = $blazies->get('media.instance');

    /** @var \Drupal\image\Plugin\Field\FieldType\ImageItem $item */
    if (!$entity && $item) {
      if (method_exists($item, 'getEntity')) {
        $entity = $item->getEntity();
      }
      else {
        $entity = $item->entity ?? NULL;
      }
    }

    if ($entity && method_exists($entity, 'isNew')) {
      if (!$entity->isNew()) {
        $value = NULL;
        if ($link = $settings['ajax_link'] ?? NULL) {
          $value = BlazyField::getString($entity, $link, $langcode, TRUE);
        }

        if (!$value) {
          return;
        }

        if (mb_strpos($value, 'internal:') !== FALSE
          || mb_strpos($value, 'entity:') !== FALSE
          || mb_strpos($value, 'route:') !== FALSE) {
          $url = Url::fromUri($value);
        }
        else {
          $url = Url::fromUserInput($value);
        }

        if ($path = $url->setAbsolute()->toString()) {
          // @todo recheck/remove, the link widget already sanitized its input.
          $old_url = $variables['url'] ?? '';
          $box_url = $box_url ?: $old_url;
          $variables['url'] = UrlHelper::filterBadProtocol($path);
          $variables['url_attributes']['data-b-ajax'] = UrlHelper::filterBadProtocol($value);
          $variables['url_attributes']['data-box-url'] = UrlHelper::filterBadProtocol($box_url);
        }
      }
    }
  }

  /**
   * Returns splidebox dataset.
   */
  private function toDataset(array &$attach): array {
    $blazies   = $attach['blazies'];
    $optionset = $this->getOptionset($attach);
    $count     = $blazies->get('item.count', 0) ?: $blazies->get('count', 0);
    $options   = $this->getOptions($optionset, $count, TRUE);
    $skin      = $optionset->getSkin() ?: 'default';
    $box_nav   = $attach['box_nav'] ?? $blazies->ui('extras.splidebox_nav');
    $navset    = NULL;
    $attrs     = [];
    $ajax_only = $attach['box_ajax_only'] ?? FALSE;
    $ajax_drag = $attach['box_ajax_drag'] ?? FALSE;

    if (empty($attach['skin'])) {
      $attach['skin'] = $skin;
    }

    $options['ajax'] = [
      'class' => 'is-sbox-ajax',
      'item'  => 'is-ajax-item',
      'only'  => $ajax_only,
    ];

    if ($ajax_drag) {
      $options['ajax']['drag'] = TRUE;
    }

    $dataset = [
      'count' => $count,
      'main' => $options,
    ];

    $skin                 = $attach['skin_lightbox'] ?? $skin;
    $attach['media']      = $attach['zoom'] = TRUE;
    $attach['fullscreen'] = TRUE;
    $attach['box_nav']    = $box_nav;

    // box_caption_pos: inline (ala Colorbox), or overlay (ala PhotoSwipe).
    // box_layout: any usual caption layouts, requires theming except bottom.
    // To replace media with AJAX contents only, override box_ajax_only to TRUE.
    $lightbox = [
      'skin'       => $skin,
      'layout'     => $attach['box_layout'] ?? 'bottom',
      'captionPos' => $attach['box_caption_pos'] ?? 'overlay',
      'useNav'     => !empty($box_nav),
    ];

    $settings = $lightbox;
    $settings['blazies'] = $blazies->reset($settings);

    if ($box_nav) {
      $settings['blazies']->set('is.nav', TRUE);
      $navset = Splide::loadSafely($box_nav);
      $skin = $navset->getSkin() ?: 'default';

      $settings['optionset_nav'] = $box_nav;
      $settings['skin_nav'] = $lightbox['skinNav'] = $skin;
      $dataset['nav'] = $this->getOptions($navset, $count, FALSE);
    }

    $build = [
      '#options'       => $options,
      '#optionset'     => $optionset,
      '#optionset_nav' => $navset,
      '#settings'      => $settings,
    ];

    $template = $this->toTemplate($build);
    $dom = Html::load($template);
    $divs = $dom->getElementsByTagName('div');
    if (property_exists($divs, 'length') && $divs->length > 0) {
      foreach ($divs as $node) {
        if ($class = $node->getAttribute('class')) {
          $attrs['id'] = $blazies->get('lightbox.gallery_id') ?: $blazies->get('css.id');
          if (strpos($class, 'splide-wrapper') !== FALSE) {
            $attrs['wrapper']['class'] = $class;
          }
          elseif (strpos($class, 'splide--main') !== FALSE) {
            $attrs['main']['class'] = $class;
            $attrs['main']['id'] = $node->getAttribute('id');
          }
          elseif (strpos($class, 'splide--nav') !== FALSE) {
            $attrs['nav']['class'] = $class;
            $attrs['nav']['id'] = $node->getAttribute('id');
          }
          elseif (strpos($class, 'splide--default') !== FALSE) {
            $attrs['default']['class'] = $class;
            $attrs['default']['id'] = $node->getAttribute('id');
          }
        }
      }
    }

    $uls = $dom->getElementsByTagName('ul');
    if (property_exists($uls, 'length') && $uls->length > 0) {
      foreach ($uls as $node) {
        if ($class = $node->getAttribute('class')) {
          if (strpos($class, 'splide__list') !== FALSE) {
            $attrs['list']['class'] = $class;
          }
        }
      }
    }

    $dataset['attrs'] = $attrs;

    $output = array_merge($lightbox, $dataset);
    $attach['splideboxes'] = $this->settings($output);

    return $output;
  }

  /**
   * Returns a dummy lightbox template.
   */
  private function toTemplate(array $build): string {
    if (!$this->template) {
      $data = $build;
      $settings = &$data['#settings'];
      // Prevents dummy complication with navigation for now.
      $settings['dummy_template'] = TRUE;
      $settings['optionset'] = $this->getOptionsetName($settings);

      $items = [];
      foreach (['One', 'Two'] as $key) {
        $items[] = [
          'slide' => ['#markup' => $key],
        ];
      }

      $data['items'] = $items;
      $data['nav']['items'] = $items;
      $html = $this->build($data);

      // @todo use directly renderInIsolation() post blazy:2.28|3.0.6.
      if (method_exists($this, 'renderInIsolation')) {
        $this->template = $this->renderInIsolation($html);
      }
      else {
        // @phpstan-ignore-next-line
        $this->template = $this->renderer->renderPlain($html);
      }
    }

    return $this->template;
  }

}
